# Third Person Camera - Made by Mobious

import bge
import Rasterizer as R
import math
import mathutils as mathu

# constants
# Feel free to tweak these.
SENSITIVITY = .003      # mouse sensitivity
S = .8                  # mouse smoothing
INV_X = 1               # invert x-axis (set to -1 to invert)
INV_Y = 1               # invert y-axis (set to -1 to invert)
MAX_DIST = 5.0          # maximum distance from player
MIN_DIST = 1.5          # minimum distance to obstacle
RSMOOTH = 20            # camera range smoothing
DRCAP = .5              # distance restore speed cap
ZOOM_STEP = .5          # distance to zoom on mousewheel
Z_OFFSET = 2.0          # Z offset from player
PROP = 'block_camera'   # object property that blocks the camera

scene = bge.logic.getCurrentScene()

# Change 'Player' to whatever object you want the camera to track
player = scene.objects['Character']

# controls
# You can change these to WHEELUPMOUSE and WHEELDOWNMOUSE if you want.
# I didn't have a mouse with a wheel at the time, so I used E and Q
ZoomIn = bge.events.EKEY
ZoomOut = bge.events.QKEY

def main():
    
    cont = bge.logic.getCurrentController()
    own = cont.owner
    mouse = cont.sensors['Mouse']
    rear = cont.sensors['rear']
    front = cont.sensors['front']
    
    # initialize vars
    if 'x' not in own:
        own['x'] = math.pi / 2
        own['y'] = math.pi / 6
        own['dist'] = MAX_DIST
        x = R.getWindowWidth() // 2
        y = R.getWindowHeight() // 2
        own['size'] = (x,y)
        R.setMousePosition(x,y)
        own['zoom'] = MAX_DIST
        own['oldX'] = 0.0
        own['oldY'] = 0.0
        rear.range = MAX_DIST
        front.range = MAX_DIST - MIN_DIST
        rear.propName = PROP
        front.propName = PROP
        return
    
    # check and correct for camera obstructions using smoothing
    if front.positive: # obstacle in front of camera
        dist = own.getDistanceTo(front.hitPosition)
        own['dist'] -= dist + MIN_DIST / RSMOOTH
    elif rear.positive: # obstacle behind camera
        dist = own.getDistanceTo(rear.hitPosition)
        if dist > MIN_DIST:
            own['dist'] += (dist - MIN_DIST) / RSMOOTH
        else:
            own['dist'] -= (MIN_DIST - dist) / RSMOOTH
    elif own['dist'] < own['zoom']: # restore max distance
        dist = (own['zoom'] - own['dist'])
        own['dist'] += dist / RSMOOTH
    if own['dist'] > own['zoom']:
        own['dist'] -= (own['dist'] - own['zoom']) / RSMOOTH
    front.range = own['dist']

    # mouse movement
    xpos = own['size'][0]
    ypos = own['size'][1]
    x = INV_X*(xpos - mouse.position[0])
    y = -INV_Y*(ypos - mouse.position[1])
    x *= SENSITIVITY
    y *= SENSITIVITY
    
    # smooth movement
    own['oldX'] = (own['oldX']*S + x*(1-S))
    own['oldY'] = (own['oldY']*S + y*(1-S))
    x = own['oldX']
    y = own['oldY']

    # center mouse in game window
    R.setMousePosition(xpos,ypos)
    
    # zoom controls
    keys = bge.logic.mouse.events
    keys.update(bge.logic.keyboard.events)
    if keys[ZoomIn] and own['zoom'] >= MIN_DIST + ZOOM_STEP:
        own['zoom'] -= ZOOM_STEP
    elif keys[ZoomOut] and own['zoom'] <= MAX_DIST - ZOOM_STEP:
        own['zoom'] += ZOOM_STEP
        
    
    # update angles
    own['x'] += x
    own['y'] += y
    if own['x'] > 2*math.pi:
        own['x'] -= 2*math.pi
    if own['x'] < 0.0:
        own['x'] += 2*math.pi
    if own['y'] > math.pi / 2:
        own['y'] = math.pi / 2
    if own['y'] < -math.pi / 2:
        own['y'] = -math.pi / 2

    # calculate and set new camera position
    own['tdist'] = own['dist']*math.fabs(math.cos(own['y']))
    own['sdist'] = own['dist']*math.fabs(math.cos(own['x']))
    zshift = own['dist']*math.sin(own['y'])
    z = player.position[2] + zshift + Z_OFFSET
    yshift = own['tdist']*math.sin(own['x'])
    y = player.position[1] + yshift
    xshift = own['tdist']*math.cos(own['x'])
    x = player.position[0] + xshift
    own.worldPosition = [x,y,z]
    
    # calculate and set camera angle
    x = -own['y'] + math.pi / 2
    y = own['x'] + math.pi / 2
    v = mathu.Vector((x,0,y))
    own.localOrientation = v
    player.localOrientation = ((math.pi,math.pi,y))
    rig = scene.objects['metarig']
    rig.channels['spine'].rotation_mode = bge.logic.ROT_MODE_XYZ
    rig.channels['spine'].rotation_euler = (-x+math.pi/2,0,0 )
    rig.update()